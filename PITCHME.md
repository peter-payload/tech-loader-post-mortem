---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

# Tech Loader Post Mortem


---?include=template/md/singleton/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md



---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md



---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---
@title[The Template Docs]

@snap[west headline span-100]
GitPitch<br>*The Template @css[text-orange](End) ;)*
@snapend

@snap[south docslink span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend
