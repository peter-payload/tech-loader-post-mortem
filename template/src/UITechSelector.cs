using MonsterLove.StateMachine;
using Payload.UI.Commands;
#if !UNITY_CONSOLES
using Payload.UI.Commands.Steam;
#endif
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITechSelector : MonoBehaviour, IMoveHandler, ISubmitHandler, ICancelHandler, IUIExtraButtonHandler2
{
	// public inspector variables ////////////////////
	[SerializeField]
	RectTransform m_ContentLayout;
	[SerializeField]
	UIPreset m_ImagePrefab;

	[SerializeField]
	Text m_LoadingInfo;

	[SerializeField]
	Text m_SelectedTechName;
	[SerializeField]
	Text m_SelectedTechCreator;
	[SerializeField]
	Text m_SelectedTechPrice;

	[SerializeField]
	Button m_SelectButton;

	[SerializeField]
	bool m_DoubleClickSelectsItem = true;

	[SerializeField]
	RectTransform m_UITabsPanel;

	[SerializeField]
	RectTransform m_BottomPanel;

	[SerializeField]
	Button m_SteamButton;

	[SerializeField]
	TechLocations m_DefaultSelectedTab = TechLocations.Local;

	[SerializeField]
	bool m_ResetDefaultTabOnHide = false;

	[SerializeField]
	ScrollRect m_GridScrollRect = null;

	[SerializeField]
	UITechLoaderHUD m_UITechLoaderHUD;

	[SerializeField]
	GameObject[] m_HiddenUIOnConsole;

	[SerializeField]
	UIUndiscoveredBlocks m_UndiscoveredBlocks;

	// public accessors //////////////////////////////
	public bool IsVisible { get { return gameObject.activeSelf; } }

	public TechLocations CurrentTab { get { return m_FSM.State; } }

	public delegate void SelectionEvent(Snapshot capture, int techCost);
	public delegate bool CanAcceptTechCallback(TechData techData, int techCost);

	// events ////////////////////////////////////////
	public SelectionEvent OnListSelectEvent;
	public SelectionEvent OnSelectionAcceptedEvent;
	public CanAcceptTechCallback CanSelectTechCallback;

	// private members ///////////////////////////////
	List<TechUIElement> m_MyImages;
#if !UNITY_CONSOLES
	TwitterAPI.TweetWithMediaDataThreaded m_FetchedTweets;
#endif
	SnapshotCollectionDisk m_SnapshotsDisk;
	SnapshotCollectionTwitter m_SnapshotsTwitter = null;
	SnapshotCollectionSteam m_SnapshotsSteam;

	Inventory m_BlockInventory = null;

	bool m_FinishedFetchingVehicleData;// Fetched Tech raw data from disk/twitter?
	bool m_FinishedLoadingCaptures;// Loaded all techs into textures and added to list?
	bool m_TwitterLoginFailed;
	bool m_TwitterAuthPending;

	CommunityTagGroup m_CommunityTagGroup = CommunityTagGroup.Default;

#if !UNITY_CONSOLES
	CommandOperation<SteamDownloadData> m_SteamGetTechList;
	SteamDownloadData m_SteamItems;
#endif

	// Keep track of what we've loaded already, and defer loading to one per frame
	int m_NumTweetsDecoded;
	int m_NumCapturesAdded;

	StateMachine<TechLocations> m_FSM;
	TechLocations m_CurrentTab;
	TechLocations m_PreviousTab = TechLocations.Null;
	Snapshot m_SelectedTech = null;
	int m_SelectedTechCost = 0;

	StateMachine<TwitterStates> m_TwitterFSM;
	StateMachine<SteamStates> m_SteamFSM;

	DoubleClickListener m_DoubleClickListener;

	ToggleGroup m_ButtonToggleGroup;
	List<UITechSelectorTabHelper> m_UITabHelpers = new List<UITechSelectorTabHelper>();

	Toggle m_PreviousSnapShotButton;

	UIShopTechSelect.TechBlockCountCache m_SelectedTechBlockCache = new UIShopTechSelect.TechBlockCountCache();

	// TODO: Expose this in future to allow new groups to be defined by design?
	static string[] m_CommunityHashtag = new string[]
	{
		"#myTerraTech",
		"#myTerraTechSumo"
	};

	// types /////////////////////////////////////////
	public enum TechLocations
	{
		Local,
		Twitter,
		Steam,
		Null,
	}


	public enum TwitterStates
	{
		TwitterIdle,
		TwitterAuth,
		TwitterLoading,
		TwitterComplete,
	}

	public enum SteamStates
	{
		SteamIdle,
		SteamLoading,
		SteamPopulate,
		SteamComplete
	}

	public enum CommunityTagGroup
	{
		Default = 0,
		Sumo
	}

	struct TechUIElement
	{
		public UIPreset uiPreset;
		public Toggle toggleButton;
		public UnityAction<bool> clickHandler;
	}

	// public interface ///////////////////////////////////////
	public void Show()
	{
		if (!gameObject.activeSelf)
		{
			gameObject.SetActive(true);

			TechLocations target = m_PreviousTab;
			if (m_PreviousTab == TechLocations.Null) //First time or m_ResetDefaultTabOnHide is true
			{
				target = m_DefaultSelectedTab;
			}
			ShowTab(target);

			// configure which tabs are enabled
			for (int i = 0; i < m_UITabHelpers.Count; i++)
			{
				var tabHelper = m_UITabHelpers[i];
				bool active;
				switch (tabHelper.TargetTab)
				{
					case TechLocations.Local:
					{
						active = true;
						break;
					}

					case TechLocations.Steam:
					{
						active = ManSteamworks.inst.Workshop.Enabled;
						break;
					}

					case TechLocations.Twitter:
					{
						active = SKU.TwitterEnabled;
						break;
					}

					default:
					{
						Debug.AssertFormat(false, "Unhandled tech location type: {0}", tabHelper.TargetTab);
						active = false;
						break;
					}
				}
				tabHelper.gameObject.SetActive(active);
			}

			ManInput.inst.SetControllerMapsForUI(this, true, UIInputMode.FullscreenUI);
			ManNavUI.inst.AddAndSelectEntryTarget(this.gameObject);

			UpdateJoypadUI();
		}
	}

	public void Hide()
	{
		if (gameObject.activeSelf)
		{
			ShowTab(TechLocations.Null);

			if (m_ResetDefaultTabOnHide)
			{
				m_PreviousTab = TechLocations.Null;
			}

			gameObject.SetActive(false);

			m_PreviousSnapShotButton = null;

			ManInput.inst.SetControllerMapsForUI(this, false, UIInputMode.FullscreenUI);
			ManNavUI.inst.ForgetEntryTarget(this.gameObject);
		}
	}

	public void SetInventory(Inventory availableInventory)
	{
		m_BlockInventory = availableInventory;
	}

	public void SetCommunitySource(CommunityTagGroup tagGroup)
	{
		m_CommunityTagGroup = tagGroup;
	}

	public void ShowTab(TechLocations targetTab)
	{
		if (m_FSM.State == targetTab)
		{
			//Already showing the target tab. Do nothing
			return;
		}
		// Reset selection
		HighlightTech(null);

		// Tear down current (old) tab and all its data
		// TODO: Evaluate what we can retain between tabs to prevent GC
		ClearTab();

		m_NumCapturesAdded = 0;

		m_FSM.ChangeState(targetTab);
	}

	// state machine /////////////////////////////////////////

	//Local Tab
	void Local_Enter()
	{
		//Local techs are cached on start up, so we can display them immediately 
		m_SnapshotsDisk = ManScreenshot.inst.GetSnapshotCollectionDisk();
	}

	void Local_Update()
	{
		if (LoadOneTechPerFrameIntoSprites(m_SnapshotsDisk))
		{
			return;
		}
	}

	//Twitter Tab
	void Twitter_Enter()
	{
		m_FinishedFetchingVehicleData = false;
		m_FinishedLoadingCaptures = false;
		m_TwitterLoginFailed = false;

		//Kick off twitter operation 
		m_TwitterFSM.ChangeState(TwitterStates.TwitterAuth);

	}

	void TwitterAuth_Enter()
	{
#if !UNITY_CONSOLES
		m_TwitterAuthPending = true;
		TwitterAuthenticationUIHandler.inst.TryAuthenticateAsync(TwitterAuth_OnComplete); //Start thread work
#endif
	}

	void TwitterAuth_OnComplete(bool loggedIn)
	{
		//Thread work is done
		m_TwitterAuthPending = false;
		m_TwitterLoginFailed = !loggedIn;
	}

	void TwitterAuth_Update()
	{
		//Resynchronise threads
		if (!m_TwitterAuthPending)
		{
			if (!m_TwitterLoginFailed)
			{
				m_TwitterFSM.ChangeState(TwitterStates.TwitterLoading);
			}
			else
			{
				m_TwitterFSM.ChangeState(TwitterStates.TwitterComplete);
			}
		}
	}

	void TwitterAuth_Exit()
	{
#if !UNITY_CONSOLES
		if (m_TwitterAuthPending)
		{
			m_TwitterAuthPending = false;
			TwitterAuthenticationUIHandler.inst.CancelAsync(); //Cancel thread work
		}
#endif
	}

	void TwitterLoading_Enter()
	{
#if UNITY_CONSOLES
		// Avoid unused warnings
		if ( m_SnapshotsTwitter==null || m_SteamFSM==null || m_CommunityTagGroup!=CommunityTagGroup.Default || m_CommunityHashtag!=null ) {
			// Dummy
		}
#else
		var modeRestriction = ManGameMode.inst.NextModeSetting.m_LoadVehicleModeRestriction;
		var submodeRestriction = ManGameMode.inst.NextModeSetting.m_LoadVehicleSubModeRestriction;
		var userRestriction = ManGameMode.inst.NextModeSetting.m_LoadVehicleUserDataRestriction;

		m_SnapshotsTwitter = new SnapshotCollectionTwitter(modeRestriction, submodeRestriction, userRestriction);

		m_FinishedFetchingVehicleData = false;

		//Request Tweets from Twitter API. Uses a worker thread
		m_FetchedTweets = new TwitterAPI.TweetWithMediaDataThreaded();
		string hashtag = m_CommunityHashtag[(int)m_CommunityTagGroup];
		TwitterAPI.inst.RetrieveTaggedTweetsAsync(hashtag, false, m_FetchedTweets, TwitterLoading_RequestCompleted);

		m_NumTweetsDecoded = 0;
#endif
	}

	//While loading, interleave the work of updating the UI and parsing tweets to spread the work
	void TwitterLoading_Update()
	{
		//Are there any available sprites to add to UI from the Snapshot Queue? 
		if (LoadOneTechPerFrameIntoSprites(m_SnapshotsTwitter))
		{
			return;
		}

		//Are there any available snapshots to parse from the Tweet queue?
		if (DecodeTweet(m_SnapshotsTwitter))
		{
			return;
		}

		//If our queues are empty, check if async loading is done
		if (m_FinishedFetchingVehicleData)
		{
			m_TwitterFSM.ChangeState(TwitterStates.TwitterComplete);
		}
	}

	void TwitterLoading_RequestCompleted()
	{
		m_FinishedFetchingVehicleData = true;
	}

	void TwitterLoading_Exit()
	{
#if !UNITY_CONSOLES
		if (!m_FinishedFetchingVehicleData)
		{
			TwitterAPI.inst.Cancel();
		}
#endif
	}

	void TwitterComplete_Enter()
	{
		m_FinishedLoadingCaptures = true;
	}

	void Twitter_Exit()
	{
		//Make sure pending API requests are cancelled
		m_TwitterFSM.ChangeState(TwitterStates.TwitterIdle);
	}

#if !UNITY_CONSOLES
	//Steam Tab 
	void Steam_Enter()
	{
		m_FinishedLoadingCaptures = false;

		if (m_SteamButton != null)
		{
			m_SteamButton.gameObject.SetActive(false);
		}

		//Kick off steam operation
		m_SteamFSM.ChangeState(SteamStates.SteamLoading);
	}

	void SteamLoading_Enter()
	{
		//TODO: Handle paging

		if (m_SteamGetTechList == null)
		{
			m_SteamGetTechList = new CommandOperation<SteamDownloadData>();

			var downloadQuery = new SteamCreateQueryCommand();

			m_SteamGetTechList.Add(downloadQuery);
		}


		if (m_SnapshotsSteam == null)
		{
			m_SnapshotsSteam = new SnapshotCollectionSteam(null, null, null, null);
		}
		m_SnapshotsSteam.Clear();

		m_SteamGetTechList.Completed += SteamLoading_OnComplete;
		SteamDownloadData data = SteamDownloadData.Create(SteamItemCategory.Techs);
		m_SteamGetTechList.Execute(data);
	}

	void SteamLoading_OnComplete(SteamDownloadData data)
	{
		m_SteamItems = data;
		if (data.HasAnyItems)
		{
			//Debugging
			//
			//	for (int i = 0; i < data.m_Items.Count; i++)
			//	{
			//		var details = data.m_Items[i].m_Details;
			//		var url = data.m_Items[i].m_PreviewURL;
			//		Debug.LogFormat("Steam Item title {0} file {1} pv File {2} file name {3} desc {4} tags {5} tagsShrt {6} url {7} userID {8} created {9} updated {10} flscore0 {11} user Terms {12} url {13}",
			//			details.m_rgchTitle,
			//			details.m_hFile,
			//			details.m_hPreviewFile,
			//			details.m_pchFileName,
			//			details.m_rgchDescription,
			//			details.m_rgchTags,
			//			details.m_bTagsTruncated,
			//			details.m_rgchURL,
			//			details.m_ulSteamIDOwner,
			//			details.m_rtimeCreated,
			//			details.m_rtimeUpdated,
			//			details.m_flScore,
			//			details.m_bAcceptedForUse,
			//			url
			//			);
			//	}

			m_SteamFSM.ChangeState(SteamStates.SteamPopulate);
		}
		else
		{
			m_SteamFSM.ChangeState(SteamStates.SteamComplete);
		}
	}

	void SteamLoading_Exit()
	{
		m_SteamGetTechList.Completed -= SteamLoading_OnComplete;
	}

	void SteamPopulate_Enter()
	{
		if (m_SteamItems.HasAnyItems)
		{
			for (int i = 0; i < m_SteamItems.m_Items.Count; i++)
			{
				var item = m_SteamItems.m_Items[i];

				//TODO: Simple pooling for opearations
				var getItemOperation = new CommandOperation<SteamDownloadItemData>();

				getItemOperation.AddConditional(SteamConditions.CheckItemNeedsDownload, new SteamItemDownloadCommand());
				getItemOperation.AddConditional(SteamConditions.CheckWaitingForDownload, new SteamItemWaitForDownloadCommand());
				getItemOperation.Add(new SteamItemGetDataFile());
				getItemOperation.Add(new SteamLoadMetaDataCommand());
				getItemOperation.Add(new SteamLoadPreviewImageCommand());
				getItemOperation.Add(new SteamItemParseSnapshot());

				getItemOperation.Completed += (d) =>
				{
					m_SnapshotsSteam.AddSnapshot(d.m_Snaphsot);
				};

				getItemOperation.Execute(item);

			}
		}


	}

	void SteamPopulate_Update()
	{
		if (LoadOneTechPerFrameIntoSprites(m_SnapshotsSteam))
		{
			return;
		}

		//TODO: Exit when all pending operations are complete
	}

	void SteamComplete_Enter()
	{
		//if no items are loaded, text will update based on this flag
		m_FinishedLoadingCaptures = true;

		//Show a button to link to the steam workshop
		if (!m_SteamItems.HasAnyItems)
		{
			if (m_SteamButton != null) { m_SteamButton.gameObject.SetActive(true); }
			if (m_BottomPanel != null) { m_BottomPanel.gameObject.SetActive(false); }
		}
	}

	void Steam_Exit()
	{
		if (m_SteamButton != null) { m_SteamButton.gameObject.SetActive(false); }
		if (m_BottomPanel != null) { m_BottomPanel.gameObject.SetActive(true); }

		m_SteamFSM.ChangeState(SteamStates.SteamIdle);
	}
#endif

	// private methods ///////////////////////////////////////

	//Compiler isn't smart enough, need to give it some hints with our type constraints
	bool LoadOneTechPerFrameIntoSprites<TSnap>(SnapshotCollection<TSnap> capCollection) where TSnap : Snapshot, new()
	{
		while (m_NumCapturesAdded < capCollection.Snapshots.Count)
		{
			Snapshot capture = capCollection.Snapshots[m_NumCapturesAdded];
			++m_NumCapturesAdded;

			AddListElementForLoadedTech(capture);
			return true;
		}

		return false;
	}

	bool DecodeTweet(SnapshotCollectionTwitter snapshotCollection)
	{
#if !UNITY_CONSOLES
		Debug.Assert(m_FetchedTweets != null, "m_FetchedTweets is null, has the Twitter API been called correctly?");

		lock (m_FetchedTweets.m_Lock)
		{
			// Wait on the fetch count since we may be waiting on threaded data
			if (m_NumTweetsDecoded < m_FetchedTweets.m_Links.Count)
			{
				TwitterAPI.TweetWithMedia tm = m_FetchedTweets.m_Links[m_NumTweetsDecoded];

				// Convert the bye array into a Texture2D Image
				Texture2D image = new Texture2D(4, 4);
				image.LoadImage(tm.ImageArray);

				// Create a DecodedCapture from this image in m_LoadedCaptures
				SnapshotTwitter snapshot;
				snapshotCollection.TryAddFromImage(image, tm.ScreenName, tm.ProfileImageUrl, tm.TweetID, out snapshot);

				++m_NumTweetsDecoded;
				return true;
			}
		}
#endif
		return false;
	}

	static bool CanBeMadeWithInventory(TechData techData, Inventory inventory)
	{
		bool sufficientBlocksForTech = true;

		int presetBlockCount = techData.m_BlockSpecs.Count;
		Dictionary<int, int> remainingBlocks = new Dictionary<int, int>(presetBlockCount); // BlockTypes

		for (int i = 0; i < presetBlockCount; ++i)
		{
			TankPreset.BlockSpec spec = techData.m_BlockSpecs[i];
			int blockType = (int)spec.m_BlockType;

			int numBlocksOfType;
			if (!remainingBlocks.TryGetValue(blockType, out numBlocksOfType))
			{
				numBlocksOfType = inventory.GetBlockQuantity(spec.m_BlockType);
				remainingBlocks.Add(blockType, numBlocksOfType);
			}

			if (numBlocksOfType == 0)
			{
				sufficientBlocksForTech = false;
				break;
			}

			if (numBlocksOfType != Inventory.BlockCount.INFINITE_QUANTITY)
			{
				// 'consume' block and keep going
				remainingBlocks[blockType] = numBlocksOfType - 1;
			}
		}
		return sufficientBlocksForTech;
	}

	void AddListElementForLoadedTech(Snapshot capture)
	{
		UIPreset preset = m_ImagePrefab.Spawn() as UIPreset;

		//Expensive. Ideally we want to only do this once per frame
		m_SelectedTechBlockCache.RecordBlockData(capture.techData, m_BlockInventory);

		preset.SetData(capture, m_SelectedTechBlockCache.Unavailable);

		preset.transform.SetParent(m_ContentLayout, false);
		preset.transform.localScale = Vector3.one;

		// Add click handler
		Toggle techButton = preset.gameObject.GetComponentInChildren<Toggle>();
		techButton.isOn = false;// Set toggle off before adding to the group (it may prevent switching off)!
		techButton.group = m_ButtonToggleGroup;

		TechUIElement uiElement = new TechUIElement()
		{
			uiPreset = preset,
			toggleButton = techButton,
			// This delegate needs to be declared in a separate function (/scope) from the Coroutine iterator as it will otherwise not capture the variable (preset) properly!
			clickHandler = delegate (bool toggledOn) { if (toggledOn) { OnTechPresetButtonClicked(capture); } }
		};

		techButton.onValueChanged.AddListener(uiElement.clickHandler);

		m_MyImages.Add(uiElement);
	}

	void RefreshListAfterDelete()
	{
		ClearTab();
		Local_Enter();
		EventSystem.current.SetSelectedGameObject(gameObject);
	}

	void UpdateSelectButtonState()
	{
		if (m_SelectButton != null)
		{
			m_SelectButton.interactable = CanAcceptCurrentSelection();
		}
	}

	void UpdateLoadingInfoText()
	{
		if (m_LoadingInfo != null)
		{
			if (m_MyImages == null || m_MyImages.Count == 0)
			{
				if (!m_LoadingInfo.gameObject.activeSelf)
				{
					m_LoadingInfo.gameObject.SetActive(true);
				}

				if (CurrentTab == TechLocations.Twitter)
				{
					if (m_FinishedLoadingCaptures || m_TwitterLoginFailed)
					{
						// TODO: Twitter not enabled - specific string?
						m_LoadingInfo.text = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.MenuMain, (int)LocalisationEnums.MenuMain.menuLoadingNotFound);
					}
					else
					{
						m_LoadingInfo.text = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.MenuMain, (int)LocalisationEnums.MenuMain.menuLoadingFromTwitter);
					}
				}
				else if (CurrentTab == TechLocations.Steam)
				{
					if (m_FinishedLoadingCaptures)
					{
						m_LoadingInfo.text = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.SteamWorkshop, (int)LocalisationEnums.SteamWorkshop.NoItems);
					}
					else
					{
						m_LoadingInfo.text = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.MenuMain, (int)LocalisationEnums.MenuMain.menuLoadingVehicles);
					}
				}
				else
				{
					m_LoadingInfo.text = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.MenuMain, (int)LocalisationEnums.MenuMain.menuLoadingNotFound);
				}
			}
			else if (m_LoadingInfo.gameObject.activeSelf)
			{
				m_LoadingInfo.gameObject.SetActive(false);
			}
		}
	}

	void ClearTab()
	{
		// Make sure to despawn all the unnecessary image objects
		if (m_MyImages != null)
		{
			for (int i = m_MyImages.Count - 1; i >= 0; --i)
			{
				// Remove listener
				m_MyImages[i].toggleButton.group = null;
				m_MyImages[i].toggleButton.onValueChanged.RemoveListener(m_MyImages[i].clickHandler);

				m_MyImages[i].uiPreset.transform.SetParent(null, false);
				m_MyImages[i].uiPreset.Recycle();
				m_MyImages.RemoveAt(i);
			}
			m_MyImages.Clear();
		}

		UpdateLoadingInfoText();
	}

	bool CanAcceptCurrentSelection()
	{
		TechData selectedTech = (m_SelectedTech != null ? m_SelectedTech.techData : null);

		if (CanSelectTechCallback != null && !CanSelectTechCallback(selectedTech, m_SelectedTechCost))
		{
			return false;
		}

		if(selectedTech == null)
		{
			return false;
		}

		//Race condition - double click beats HighlightTech(). Ensure block data up to date
		if (m_SelectedTechBlockCache.m_Tech != selectedTech) 
		{
			m_SelectedTechBlockCache.RecordBlockData(m_SelectedTech.techData, m_BlockInventory);
		}

		if(m_SelectedTechBlockCache.Unavailable)
		{
			return false;
		}

		return true;
	}

	// MonoBehaviour methods /////////////////////////
	void Update()
	{
		UpdateLoadingInfoText();
	}

	// events //////////////////////////////////////
	void OnTechPresetButtonClicked(Snapshot capture)
	{
		bool wasDoubleClick = false;

		if (m_DoubleClickSelectsItem)
		{
			// Update state of double click
			bool canDoubleClick = (capture == m_SelectedTech);
			wasDoubleClick = m_DoubleClickListener.WasClickEventDoubleClick(canDoubleClick);
		}

		if (wasDoubleClick)
		{
			if (CanAcceptCurrentSelection())
			{
				SelectTech();
			}
		}
		else
		{
			HighlightTech(capture);
		}
	}

	void HighlightTech(Snapshot capture)
	{
		m_SelectedTech = capture;
		if (m_SelectedTech != null)
		{
			TechData techData = capture.techData;

			if (m_SelectedTechName != null)
			{
				m_SelectedTechName.text = techData.Name;
			}
			if (m_SelectedTechCreator != null)
			{
				m_SelectedTechCreator.text = capture.creator;
			}

			const bool silentFail = true;
			m_SelectedTechCost = RecipeManager.inst.GetTechPrice(techData, silentFail);
			if (m_SelectedTechPrice != null)
			{
				string costText = Localisation.inst.GetLocalisedString(LocalisationEnums.StringBanks.Purchasing, (int)LocalisationEnums.Purchasing.ItemCost);
				m_SelectedTechPrice.text = string.Format(costText, m_SelectedTechCost);
			}

			//Raise event to notify highlight has changed
			if (OnListSelectEvent != null)
			{
				OnListSelectEvent.Invoke(m_SelectedTech, m_SelectedTechCost);
			}

			m_SelectedTechBlockCache.RecordBlockData(techData, m_BlockInventory);

			if (m_UndiscoveredBlocks != null)
			{
				
				m_UndiscoveredBlocks.SetActive(m_SelectedTechBlockCache.Unavailable);

				if (m_SelectedTechBlockCache.Unavailable)
				{
					m_UndiscoveredBlocks.PopulateItems(m_SelectedTechBlockCache);
				}
			}
		}
		else
		{
			if (m_SelectedTechName != null)
			{
				m_SelectedTechName.text = string.Empty;
			}
			if (m_SelectedTechCreator != null)
			{
				m_SelectedTechCreator.text = string.Empty;
			}

			m_SelectedTechCost = 0;
			if (m_SelectedTechPrice != null)
			{
				m_SelectedTechPrice.text = string.Empty;
			}

			if(m_UndiscoveredBlocks != null)
			{
				m_UndiscoveredBlocks.SetActive(false);
			}
		}

		// enable Load button based on selection state
		UpdateSelectButtonState();
	}

	void SelectTech()
	{
		if (OnSelectionAcceptedEvent != null)
		{
			OnSelectionAcceptedEvent.Invoke(m_SelectedTech, m_SelectedTechCost);
		}
	}

	void OnDiskCollectionChanged(SnapshotCollection<SnapshotDisk> collection)
	{
		//TODO: Update the list
		//if (m_SnapshotsDisk != null)
		//{
		//	ShowTab(TechLocations.Local);
		//}
	}


	void OnTabChanged(TechLocations currentTab)
	{
		//Keep track of the previous. Neccessary to restore the last visible tab when closed
		m_PreviousTab = m_FSM.LastState;
	}

	// Interface ////////////////////////////////
	public void OnSubmit(BaseEventData eventData)
	{
		if (gameObject.activeSelf)
		{
			if (CanAcceptCurrentSelection())
			{
				SelectTech();
			}

			eventData.Use();
		}
	}

	public void OnCancel(BaseEventData eventData)
	{
		if (gameObject.activeSelf)
		{
			if (m_UITechLoaderHUD != null)
			{
				m_UITechLoaderHUD.CloseTechLoader();
			}
			eventData.Use();
		}
	}

	public void OnMove(AxisEventData eventData)
	{
		if (gameObject.activeSelf)
		{
			MoveSelection(eventData.moveVector);
			eventData.Use();
		}
	}

	public void OnUIExtraButton2(BaseEventData eventData)
	{
		if (gameObject.activeSelf && m_SelectedTech != null)
		{
			// Meh.  There's an assumption here that the name of the tech is the SAME as the name of the saved file.
			// Poppycock!  Especially, in the case of Autosaved Snapshots!  These have a timestamp type suffix.  :-|
			string techName = m_SelectedTechName.text;
			if (m_SelectedTech != null && (m_SelectedTech is SnapshotDisk))
			{
				SnapshotDisk pSD = (m_SelectedTech as SnapshotDisk);
				d.Assert(pSD != null, "ASSERT - Selected Capture is NOT a SnapshotDisk!  Type=" + m_SelectedTech.GetType().Name);
				techName = pSD.GetFileName();
			}

			// Delete the blighter!
			ManScreenshot.inst.DeleteSnapshotRender(techName, RefreshListAfterDelete);

			eventData.Use();
		}
	}

	// Private functions ////////////////////////////////
	void UpdateJoypadUI()
	{
		bool useGamepadUI = ManInput.inst.IsGamepadUseEnabled();

		foreach (var item in m_HiddenUIOnConsole)
		{
			item.gameObject.SetActive(!useGamepadUI);
		}

		if (m_UITabsPanel != null)
		{
			m_UITabsPanel.gameObject.SetActive(!useGamepadUI);
		}
	}

	void MoveSelection(Vector2 dir)
	{
		TechUIElement selectedImage = FindSelectedImage(m_SelectedTech);

		if (selectedImage.Equals(default(TechUIElement)))
		{
			TrySelectFirstItem();
		}
		else
		{
			var nextImage = GetNextImageInDirection(selectedImage, dir);
			HighLightButton(nextImage.toggleButton);
		}
	}

	void HighLightButton(Toggle btn)
	{
		if (m_PreviousSnapShotButton == btn)
		{
			return;
		}

		UIHelpers.VertScrollToItem(m_GridScrollRect.content, btn.GetComponent<RectTransform>(), m_GridScrollRect.viewport.rect.height);

		btn.isOn = true;
		m_PreviousSnapShotButton = btn;
	}

	void TrySelectFirstItem()
	{
		if (m_MyImages.Count > 0)
		{
			HighLightButton(m_MyImages[0].toggleButton);
		}
	}

	TechUIElement FindSelectedImage(Snapshot m_Tech)
	{
		foreach (var item in m_MyImages)
		{
			if (item.uiPreset.GetData() == m_Tech)
			{
				return item;
			}
		}

		return default(TechUIElement);
	}

	TechUIElement GetNextImageInDirection(TechUIElement fromUI, Vector2 dir)
	{
		TechUIElement nearestUI = fromUI;
		float nearestDist = float.MaxValue;
		var perpendicularDir = new Vector2(dir.y, -dir.x);
		foreach (var image in m_MyImages)
		{
			var fromPosition = fromUI.uiPreset.GetComponent<RectTransform>().position;
			var imagePosition = image.uiPreset.GetComponent<RectTransform>().position;

			var relPos = imagePosition - fromPosition;
			var distParallel = Vector2.Dot(relPos, dir);
			var distPerpendicular = Mathf.Abs(Vector2.Dot(relPos, perpendicularDir));
			if (distParallel > 0)
			{
				var dist = distParallel + distPerpendicular * 2.0f;
				if (dist < nearestDist)
				{
					nearestUI = image;
					nearestDist = dist;
				}
			}
		}

		return nearestUI;
	}

	// Pool interface ////////////////////////////////
	void OnPool()
	{
		const int defaultListSize = 10;
		m_MyImages = new List<TechUIElement>(defaultListSize);
		m_ImagePrefab.CreatePool(defaultListSize);

		if (m_SelectButton != null)
		{
			m_SelectButton.onClick.AddListener(SelectTech);
		}

		m_DoubleClickListener = new DoubleClickListener();

		m_ButtonToggleGroup = m_ContentLayout.GetComponent<ToggleGroup>();

		m_FSM = StateMachine<TechLocations>.Initialize(this, TechLocations.Null);
		m_FSM.Changed += OnTabChanged;

		m_TwitterFSM = StateMachine<TwitterStates>.Initialize(this, TwitterStates.TwitterIdle);
		m_SteamFSM = StateMachine<SteamStates>.Initialize(this, SteamStates.SteamIdle);

		if (m_UITabsPanel != null) //Collect tabs so we can optionally disable the steam tab.
		{
			m_UITabsPanel.GetComponentsInChildren(true, m_UITabHelpers);
		}

#if UNITY_EDITOR
		Debug.Assert(m_ImagePrefab.gameObject.GetComponentInChildren<Toggle>() != null, "UITechSelector - Image Prefab does not have a Toggle button component!");
#endif
	}

	void OnSpawn()
	{
		gameObject.SetActive(false);

		if (m_SteamButton != null) { m_SteamButton.gameObject.SetActive(false); }
		if (m_BottomPanel != null) { m_BottomPanel.gameObject.SetActive(true); }
	}

	void OnRecycle()
	{
		//if(m_SnapshotsDisk != null) m_SnapshotsDisk.Changed.Unsubscribe(OnDiskCollectionChanged);

		OnListSelectEvent = null;
		OnSelectionAcceptedEvent = null;
		CanSelectTechCallback = null;

		m_PreviousTab = TechLocations.Null;
		m_BlockInventory = null;
		m_SelectedTech = null;
	}
}